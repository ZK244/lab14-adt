#include <iostream>
#include <ostream>

class Date{
private:
  int day;
  int month;
  int year;
public:
   Date(int day , int month , int year) {
   this->day = day;
   this->month = month;
   this->year = year;
   }
   int getday() const { return this->day; }
   int getmonth() const { return this->month; }
   int getyear() const { return this->year; }
};

bool operator <<(const Date& lhs, const Date& rhs) {

 if (lhs.getyear() < rhs.getyear()) {
 return true;
 } else if (lhs.getyear() == rhs.getyear() &&
 lhs.getmonth() < rhs.getmonth()) {
 return true;
 } else if (lhs.getyear() == rhs.getyear() &&
 lhs.getmonth() == rhs.getmonth() &&
 lhs.getday() < rhs.getday()) {
 return true;
 }
 return false;
}

 std :: ostream& operator <<(std :: ostream& out, Date date) {
 out << date.getday() << '-' << date.getday() << '-' <<date.getday();
 return out;
}

void display(Date &date){
   std::cout<<date.getday()<<"-"<<date.getmonth()<<"-"<<date.getyear()<<"\n";
}

bool compare(const Date& lhs , const Date& rhs){
  if (lhs.getyear() < rhs.getyear()) {
    return true;
  } 
  else if (lhs.getyear() == rhs.getyear() && lhs.getmonth() < rhs.getmonth()) {
    return true;
  }
  else if (lhs.getyear() == rhs.getyear() && lhs.getmonth() == rhs.getmonth() && lhs.getday() < rhs.getday()) {
    return true;
  }
  return true;
}

int main(){
  Date d1(18,6,2001);
  Date d2(18,6,2001);
  Date d3(25,1,2021);
  display(d1);
  std::cout<<compare(d1,d2);
  std::cout<<compare(d2,d3);
}
